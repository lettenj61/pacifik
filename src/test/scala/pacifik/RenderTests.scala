package pacifik

import ammonite.ops
import ammonite.ops.Path

import utest._

object RenderTests extends TestSuite {
  def makeTestFiles(testRoot: Path): Unit = {
    ops.mkdir(testRoot / "template")
    ops.mkdir(testRoot / "template" / "nested")
    ops.write.over(
      testRoot / "template" / "a.html",
      """<!DOCTYPE html>
        |<html>
        |<head>
        |  <meta charset="UTF-8">
        |</head>
        |<body>
        |  <div id="main">
        |    <h1 class="title">Hello</h1>
        |  </div>
        |</body>
        |</html>
      """.stripMargin
    )
    ops.write.over(
      testRoot / "template" / "nested" / "deep.html",
      """<!DOCTYPE html>
        |<html lang="en">
        |<head>
        |  <meta charset="UTF-8">
        |  <meta name="viewport" content="width=device-width, initial-scale=1.0">
        |  <meta http-equiv="X-UA-Compatible" content="ie=edge">
        |  <title th:text="#{params.a}">Document</title>
        |</head>
        |<body>
        |  <main id="main" class="main">
        |    <nav class="top-nav">
        |      <h1 class="title">IT IS DEEP</h1>
        |      <h3 class="subtitle">WHOA...</h3>
        |    </nav>
        |    <div class="content">
        |      <p>OK, LEAVE</p>
        |    </div>
        |  </main>
        |</body>
        |</html>
      """.stripMargin
    )
    ops.write.over(
      testRoot / "site.conf",
      """params {
        |  a: "string"
        |  b: long long long text
        |  c: no
        |  list: ["super", "trooper"]
        |  more {
        |    "i can't believe" = "this can happen"
        |  }
        |}
      """.stripMargin
    )
  }
  val tests = Tests {
    val testDir = ops.tmp.dir(prefix = "pacifik.test")
    makeTestFiles(testDir)
    val opts = Options.loadReferenceConfig.copy(
      projectRoot = testDir
    )
    'Renderer - {
      (new Renderer).processDir(opts)
//      ops.rm(opts.projectRoot)
      opts.projectRoot
    }
  }
}
