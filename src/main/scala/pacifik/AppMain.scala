package pacifik

import org.springframework.boot.{CommandLineRunner, SpringApplication}
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.logging.{LogLevel, LoggingSystem}

@SpringBootApplication
class BootConfig extends CommandLineRunner {
  override def run(string: String*): Unit = {
    Options.parser.parse(string.toSeq, Options.loadReferenceConfig) match {
      case Some(opts) =>
        if (opts.verbose) {
          LoggingSystem.get(getClass.getClassLoader).setLogLevel("root", LogLevel.DEBUG)
        }
        opts.flag match {
          case Flag.Generate =>
            (new Renderer).processDir(opts)
          case _ =>
        }
      case None => println("No config")
    }
  }
}

object AppMain {
  def main(args: Array[String]): Unit = {
    val app = new SpringApplication(classOf[BootConfig])
    val ctx = app.run(args: _*)
    ctx.close()
  }
}
