package pacifik

import com.typesafe.config.{Config, ConfigFactory}
import ammonite.ops
import ammonite.ops.Path

case class ProjectFile(options: Options) {
  def resolve(rest: String): Path =
    ProjectFile.resolvePath(this, rest)
  def projectRoot: Path = options.projectRoot
  def templateDir: Path = resolve(options.templateDir)
  def destDir: Path = resolve(options.destination)

  def ensureDestination(): Unit = {
    if (ops.exists(destDir)) {
      ops.rm(destDir)
    }
    ops.mkdir(destDir)
  }

  def readDefaultSiteConfig: Option[Config] = {
    List(".conf", ".properties", ".json")
      .map(ext => resolve("site" + ext))
      .collectFirst {
        case file if ops.exists(file) => ConfigFactory.parseFile(file.toIO)
      }
  }
}

object ProjectFile {
  def resolvePath(opts: ProjectFile, pathname: String): Path =
    try Path(pathname) catch {
      case _: Exception => opts.projectRoot / pathname
    }
}
