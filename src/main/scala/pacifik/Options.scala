package pacifik

import ammonite.ops
import ammonite.ops.Path
import com.typesafe.config.ConfigFactory
import scopt.OptionParser

sealed trait Flag
object Flag {
  case object Init extends Flag
  case object Generate extends Flag
  case object Noop extends Flag
}

final case class Options(
  projectRoot: Path,
  verbose: Boolean,
  baseURL: String,
  destination: String,
  templateDir: String,
  staticDir: String,
  flag: Flag,
  initToDir: String
)

object Options {
  def loadReferenceConfig: Options = {
    val config = ConfigFactory.defaultReference()
    Options(
      ops.pwd,
      false,
      config.getString("baseURL"),
      config.getString("publishDir"),
      config.getString("templateDir"),
      config.getString("staticDir"),
      Flag.Noop,
      ""
    )
  }

  def parser: OptionParser[Options] = new OptionParser[Options]("pacifik") {
    head("pacifik", "0.1.0")

    help("help").abbr("h").text("print this help message")

    opt[Unit]('v', "verbose")
      .action((_, c) => c.copy(verbose = true))
      .text("if given, enable spring-boot logger")

    opt[String]('d', "destination")
      .action((x, c) => c.copy(destination = x))
      .text("where to write files (default: 'public')")

    opt[String]('t', "templateDir")
      .action((x, c) => c.copy(templateDir = x))
      .text("where to locate templates (default: 'template')")

    opt[String]('i', "init")
      .action((x, c) => c.copy(flag = Flag.Init, initToDir = x))
      .text("initialize new project")

    opt[Unit]('g', "generate")
      .action((_, c) => c.copy(flag = Flag.Generate))
      .text("generate html files")
  }
}
