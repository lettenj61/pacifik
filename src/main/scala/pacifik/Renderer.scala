package pacifik

import scala.collection.JavaConverters._
import java.io.File

import ammonite.ops
import com.typesafe.config.{Config, ConfigList, ConfigObject, ConfigValue, ConfigValueType => CVT}
import org.slf4j.LoggerFactory
import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import org.thymeleaf.spring5.dialect.SpringStandardDialect
import org.thymeleaf.templateresolver._

class Renderer {

  private lazy val logger = LoggerFactory.getLogger(classOf[Renderer])

  final def extract(cv: ConfigValue): AnyRef = {
    cv.valueType match {
      case CVT.BOOLEAN | CVT.NUMBER | CVT.STRING =>
        cv.unwrapped
      case CVT.NULL => null.asInstanceOf[AnyRef]
      case CVT.LIST => cv.asInstanceOf[ConfigList].asScala.map(extract).asJava
      case CVT.OBJECT =>
        cv.asInstanceOf[ConfigObject].asScala.mapValues(extract).asJava
    }
  }
  def makeContext(ctx: Context, siteConfig: Config): Context = {
    if (siteConfig.hasPath("params")) {
      val params = siteConfig.getObject("params").asScala
      for ((k, cv) <- params) {
        ctx.setVariable(k, extract(cv))
      }
    }
    ctx
  }

  def processDir(options: Options): Unit = {
    val proj = ProjectFile(options)

    if (ops.exists(proj.templateDir)) {
      val resolver = new FileTemplateResolver
      resolver.setPrefix(proj.templateDir.toString + File.separatorChar)
      resolver.setTemplateMode("LEGACYHTML5")

      val engine = new TemplateEngine
      engine.setDialect(new SpringStandardDialect)
      engine.setTemplateResolver(resolver)

      val ctx = new Context
      proj.readDefaultSiteConfig.foreach(sc => makeContext(ctx, sc))

      proj.ensureDestination()

      if (logger.isDebugEnabled) {
        logger.debug("Context variables: {}", ctx)
      }

      val templates = ops.ls.rec(proj.templateDir).filter(_.ext == "html")
      templates.foreach(tpl => {
        val relPath = tpl.relativeTo(proj.templateDir)
        resolver.addTemplateAlias(tpl.name, relPath.toString())
      })

      if (logger.isDebugEnabled) {
        logger.debug("Template aliases: {}", resolver.getTemplateAliases)
      }

      templates.foreach { tpl =>
        val relPath = tpl.relativeTo(proj.templateDir)
        val content = engine.process(relPath.toString, ctx)
        ops.write.over(proj.destDir / relPath, content)
      }
    }
  }
}
