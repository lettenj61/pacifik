val scalaV    = "2.12.7"
val springV   = "2.1.0.RELEASE"

lazy val root = project.in(file("."))
  .enablePlugins(JavaAppPackaging)
  .settings(
    organization    := "com.github.lettenj61",
    name            := "pacifik",
    description     := "Tiny, opinionated static page renderer with Thymeleaf",
    version         := "0.1.0-SNAPSHOT",
    scalaVersion    := scalaV,

    libraryDependencies ++= Seq(
      "org.springframework.boot" % "spring-boot-starter-thymeleaf" % springV,
      "com.typesafe" % "config" % "1.3.2",
      "net.sourceforge.nekohtml" % "nekohtml" % "1.9.22",
      "com.github.scopt" %% "scopt" % "3.7.0",
      "com.lihaoyi" %% "ammonite-ops" % "1.2.1",
      "com.lihaoyi" %% "utest"        % "0.6.5" % "test"
    ),

    testFrameworks += new TestFramework("utest.runner.Framework"),

    mainClass in (Compile, run) := Some("pacifik.AppMain")
  )
